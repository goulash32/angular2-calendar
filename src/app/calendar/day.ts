export class Day {
    date: number;
    month: string;
    year: number;
    prevMonth?: boolean;
}
