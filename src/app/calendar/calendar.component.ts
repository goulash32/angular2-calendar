import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { Day } from './day';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  private headings: string[] = [
    'Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'
  ];
  @Output() dayClicked = new EventEmitter();
  dates: Day[] = [];
  datesBefore: Day[] = [];
  datesAfter: Day[] = [];
  title: string;
  current: moment.Moment = moment().startOf('month').startOf('day');

  constructor () {}

  ngOnInit() {
    this.initMonth();
  }

  private _datesBefore(current: moment.Moment): Day[] {
    const tempCurrent = current.clone();
    const startDay = tempCurrent.startOf('month').day();
    const prevMonthStart = tempCurrent.startOf('month').subtract(startDay, 'days');

    const retDays: Day[] = [];
    for (let i = 0; i < startDay; ++i) {
      const day = new Day();
      day.date = prevMonthStart.date();
      day.month = prevMonthStart.format('MMMM');
      day.year = prevMonthStart.year();
      day.prevMonth = true;
      prevMonthStart.add(1, 'day');
      retDays.push(day);
    }

    return retDays;
  }

  private _datesAfter(current: moment.Moment): Day[] {
    const tempCurrent = current.clone();
    const endDay = tempCurrent.endOf('month').day();
    const daysLeft = 6 - endDay;
    tempCurrent.add(1, 'day');
    console.log(tempCurrent.date(), tempCurrent.month(), tempCurrent.year());

    const retDays: Day[] = [];
    for (let i = 0; i < daysLeft; ++i) {
      const day = new Day();
      day.date = tempCurrent.date();
      day.month = tempCurrent.format('MMMM');
      day.year = tempCurrent.year();
      console.log(day);
      tempCurrent.add(1, 'day');
      retDays.push(day);
    }

    return retDays;
  }

  initMonth() {
    const tempCurrent = this.current.clone();
    this.title = tempCurrent.format('MMMM, YYYY');
    this.dates = [];
    this.dates = this.dates.concat(this._datesBefore(tempCurrent));
    const month = tempCurrent.format('MMMM');
    const year = tempCurrent.year();
    for (let i = 1; i <= tempCurrent.daysInMonth(); i++) {
      const date = new Day();
      date.date = tempCurrent.date(i).date();
      date.month = month;
      date.year = year;
      this.dates.push(date);
    }
    this.dates = this.dates.concat(this._datesAfter(tempCurrent));
  }

  nextMonth() {
    this.current.add(1, 'month').startOf('month');
    this.initMonth();
  }

  prevMonth() {
    this.current.subtract(1, 'month').startOf('month');
    this.initMonth();
  }

  sendEvent(date: Day) {
    this.dayClicked.emit(date);
  }
}
