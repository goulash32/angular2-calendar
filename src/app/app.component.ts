import { Component } from '@angular/core';
import { Day } from './calendar/day';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  updateDayClicked(day: Day) {
    alert(day.date + ' ' + day.month + ', ' + day.year);
  }
}
